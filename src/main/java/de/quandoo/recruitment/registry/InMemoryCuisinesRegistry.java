package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private final Map<Customer, Set<Cuisine>> customersToCuisinesMapping = new ConcurrentHashMap<>();
    private final Map<Cuisine, Set<Customer>> cuisinesToCustomersMapping = new ConcurrentHashMap<>();

    @Override
    public void register(Customer customer, Cuisine cuisine) {
        validateCustomer(customer);
        validateCuisine(cuisine);

        store(customer, cuisine);
    }

    private void validateCustomer(Customer customer) {
        if (Objects.isNull(customer)) {
            throw new IllegalArgumentException("Customer is null");
        }
    }

    private void validateCuisine(Cuisine cuisine) {
        if (Objects.isNull(cuisine)) {
            throw new IllegalArgumentException("Cuisine is null");
        }
    }

    private void store(Customer customer, Cuisine cuisine) {
        this.cuisinesToCustomersMapping
                .computeIfAbsent(cuisine, key -> ConcurrentHashMap.newKeySet())
                .add(customer);
        this.customersToCuisinesMapping
                .computeIfAbsent(customer, key -> ConcurrentHashMap.newKeySet())
                .add(cuisine);
    }

    @Override
    public List<Customer> cuisineCustomers(Cuisine cuisine) {
        validateCuisine(cuisine);

        if (this.cuisinesToCustomersMapping.containsKey(cuisine)) {
            return new ArrayList<>(this.cuisinesToCustomersMapping.get(cuisine));
        }
        return Collections.emptyList();
    }

    @Override
    public List<Cuisine> customerCuisines(Customer customer) {
        validateCustomer(customer);

        if (this.customersToCuisinesMapping.containsKey(customer)) {
            return new ArrayList<>(this.customersToCuisinesMapping.get(customer));
        }
        return Collections.emptyList();
    }

    @Override
    public List<Cuisine> topCuisines(int n) {
        if (n <= 0) return Collections.emptyList();

        List<Cuisine> topCuisines = cuisinesToCustomersMapping
                .entrySet()
                .stream()
                .flatMap(currentMap -> {
                    int sizeOfMapSet = currentMap.getValue().size();
                    Map<Cuisine, Integer> scoreMap = new ConcurrentHashMap<>();
                    scoreMap.put(currentMap.getKey(), sizeOfMapSet);
                    return scoreMap.entrySet().stream();
                })
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .map(currentMap -> currentMap.getKey())
                .collect(Collectors.toCollection(LinkedList::new));

        return Collections.unmodifiableList(topCuisines.subList(0, (Math
                .min(topCuisines.size(), n))));
    }
}
