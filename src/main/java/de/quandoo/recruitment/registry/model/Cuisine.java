package de.quandoo.recruitment.registry.model;

import java.util.Objects;

public final class Cuisine {

    private final String name;

    public Cuisine(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Cuisine cuisine = (Cuisine) o;
        return Objects.equals(this.name,cuisine.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
