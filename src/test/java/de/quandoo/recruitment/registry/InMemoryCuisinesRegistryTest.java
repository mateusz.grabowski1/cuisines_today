package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

public class InMemoryCuisinesRegistryTest {

    //* Register Customer tests *//
    @Test
    public void registerCustomerWithCuisine() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        Cuisine cuisine = new Cuisine("French");
        Customer customer = new Customer("Tim");
        //When
        cuisinesRegistry.register(customer, cuisine);
        //Then
        assertThat(cuisinesRegistry.customerCuisines(customer)).contains(cuisine);
    }

    @Test
    public void registerCustomerWithNullCuisineArg() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        Customer customer = new Customer("tim");
        Cuisine cuisine = null;
        //When
        Throwable thrown = catchThrowable(()->cuisinesRegistry.register(customer,cuisine));
        //Then
        assertThat(thrown).hasMessage("Cuisine is null");
    }

    @Test
    public void registerCustomerWithNullCustomerArg() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        Customer customer = null;
        Cuisine cuisine = new Cuisine("Thai");
        //When
        Throwable thrown = catchThrowable(() -> { cuisinesRegistry.register(customer, cuisine);});
        //Then
        assertThat(thrown).hasMessage("Customer is null");
    }

    @Test
    public void registerCustomerWithNullCustomerAndCuisineArgs() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        Customer customer = null;
        Cuisine cuisine = null;
        //When
        Throwable thrown = catchThrowable(() -> { cuisinesRegistry.register(customer, cuisine);});
        //Then
        assertThat(thrown).hasMessage("Customer is null");
    }

    @Test
    public void shouldNotRegisterSameCuisineForCustomerTwice() {
        // Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        Cuisine cuisine = new Cuisine("French");
        Customer customer = new Customer("tim");

        // When
        cuisinesRegistry.register(customer, cuisine);
        cuisinesRegistry.register(customer, cuisine);
        List<Cuisine> customerCuisines = cuisinesRegistry.customerCuisines(customer);

        // Then
        assertThat(customerCuisines.size()).isEqualTo(1);
    }


    //* CustomersCuisines tests *//

    @Test
    public void retrieveCustomersCuisinesListForOneCustomerOneCuisine() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        Customer tim = new Customer("tim");
        Cuisine french = new Cuisine("french");
        //When
        cuisinesRegistry.register(tim, french);
        //Then
        assertThat(cuisinesRegistry.customerCuisines(tim)).contains(french);
    }

    @Test
    public void retrieveCustomersCuisinesListForOneCustomerMultipleCuisines() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        Customer tim = new Customer("tim");
        Cuisine french = new Cuisine("french");
        Cuisine german = new Cuisine("german");
        //When
        cuisinesRegistry.register(tim, french);
        cuisinesRegistry.register(tim, german);
        //Then
        assertThat(cuisinesRegistry.customerCuisines(tim)).contains(french,german);
    }

    //* CuisinesCustomer tests *//

    @Test
    public void retrieveCuisinesCustomerListForOneCuisineOneCustomer() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        Customer tim = new Customer("tim");
        Cuisine french = new Cuisine("french");
        //When
        cuisinesRegistry.register(tim, french);
        //Then
        assertThat(cuisinesRegistry.cuisineCustomers(french)).contains(tim);
    }

    @Test
    public void retrieveCuisinesCustomerListForOneCuisineMultipleCustomers() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        Customer tim = new Customer("tim");
        Cuisine french = new Cuisine("french");
        Customer horhe = new Customer("horhe");
        //When
        cuisinesRegistry.register(tim, french);
        cuisinesRegistry.register(horhe, french);
        //Then
        assertThat(cuisinesRegistry.cuisineCustomers(french)).contains(tim,horhe);
    }

    //* topCuisines Tests *//

    @Test
    public void retrieveTopCuisinesForListLengthZeroArgZero() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        //When
        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(0);
        //Then
        assertThat(topCuisines).isEmpty();
    }

    @Test
    public void retrieveTopCuisinesForListLengthNonZeroArgZero() {
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        Customer tim = new Customer("tim");
        Customer goerge = new Customer("Geroge");
        Customer rick = new Customer("rick");

        Cuisine french = new Cuisine("french");
        Cuisine german = new Cuisine("german");
        Cuisine polish = new Cuisine("polish");
        //When
        cuisinesRegistry.register(tim,french);
        cuisinesRegistry.register(goerge,french);
        cuisinesRegistry.register(rick,french);

        cuisinesRegistry.register(tim,german);
        cuisinesRegistry.register(goerge,german);

        cuisinesRegistry.register(tim, polish);

        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(0);
        //Then
        assertThat(topCuisines).isEmpty();
    }

    @Test
    public void retrieveTopCuisinesForListLengthZeroArgOne() {
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        //When
        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(1);
        //Then
        assertThat(topCuisines).isEmpty();
    }

    @Test
    public void retrieveTopCuisinesForListLengthMoreThanOneArgOne() {
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        Customer tim = new Customer("tim");
        Customer goerge = new Customer("Geroge");
        Customer rick = new Customer("rick");

        Cuisine french = new Cuisine("french");
        Cuisine german = new Cuisine("german");
        Cuisine polish = new Cuisine("polish");
        //When
        cuisinesRegistry.register(tim,french);
        cuisinesRegistry.register(goerge,french);
        cuisinesRegistry.register(rick,french);

        cuisinesRegistry.register(tim,german);
        cuisinesRegistry.register(goerge,german);

        cuisinesRegistry.register(tim, polish);

        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(1);
        //Then
        assertThat(topCuisines).startsWith(french);
    }

    @Test
    public void retrieveTopCuisinesForListLengthZeroArgGreaterThanOne() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        //When
        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(2);
        //Then
        assertThat(topCuisines).isEmpty();
    }

    @Test
    public void retrieveTopCuisinesForListLengthMoreThanOneArgGreaterThanOne() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        Customer tim = new Customer("tim");
        Customer goerge = new Customer("Geroge");
        Customer rick = new Customer("rick");

        Cuisine french = new Cuisine("french");
        Cuisine german = new Cuisine("german");
        Cuisine polish = new Cuisine("polish");
        //When
        cuisinesRegistry.register(tim,french);
        cuisinesRegistry.register(goerge,french);
        cuisinesRegistry.register(rick,french);

        cuisinesRegistry.register(tim,german);
        cuisinesRegistry.register(goerge,german);

        cuisinesRegistry.register(tim, polish);
        //When
        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(2);
        //Then
        assertThat(topCuisines).startsWith(french,german);
    }

    @Test
    public void retrieveTopCuisinesForListLengthMoreThanOneArgMinusOne() {
        //Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        Customer tim = new Customer("tim");
        Customer goerge = new Customer("Geroge");
        Customer rick = new Customer("rick");

        Cuisine french = new Cuisine("french");
        Cuisine german = new Cuisine("german");
        Cuisine polish = new Cuisine("polish");
        //When
        cuisinesRegistry.register(tim,french);
        cuisinesRegistry.register(goerge,french);
        cuisinesRegistry.register(rick,french);

        cuisinesRegistry.register(tim,german);
        cuisinesRegistry.register(goerge,german);

        cuisinesRegistry.register(tim, polish);
        //When
        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(-1);
        //Then
        assertThat(topCuisines).isEmpty();
    }

    @Test
    public void retrieveTopCuisinesForListLengthLessThenArg() {
        // Given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        Cuisine firstCuisine = new Cuisine("firstCuisine");
        Cuisine secondCuisine = new Cuisine("secondCuisine");
        Cuisine thirdCuisine = new Cuisine("thirdCuisine");
        // When
        cuisinesRegistry.register(new Customer("test1"), firstCuisine);
        cuisinesRegistry.register(new Customer("test2"), secondCuisine);
        cuisinesRegistry.register(new Customer("test3"), secondCuisine);
        cuisinesRegistry.register(new Customer("test4"), thirdCuisine);
        cuisinesRegistry.register(new Customer("test5"), secondCuisine);
        cuisinesRegistry.register(new Customer("test6"), firstCuisine);
        //Then
        assertThat(cuisinesRegistry.topCuisines(100)).size().isEqualTo(3);
    }
}