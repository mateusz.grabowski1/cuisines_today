package de.quandoo.recruitment.registry.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class CuisineTest {

    @Test
    public void getName() {
        Cuisine testCuisine = new Cuisine("French");

        assert testCuisine.getName().equals("French");
    }
}