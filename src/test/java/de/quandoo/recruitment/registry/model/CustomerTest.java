package de.quandoo.recruitment.registry.model;

import org.junit.Test;

public class CustomerTest {
    private final Customer JOHN = new Customer("John");

    @Test
    public void testCustomerNameRetrieval() {
        assert (JOHN.getUuid()).equals("John");
    }


    @Test
    public void testAssertCustomersEqual(){
        Customer john = new Customer("John");
        Customer phillip = new Customer("Phillip");

        assert john.getClass().equals(phillip.getClass());
    }

    @Test
    public void testCustomerGetUUID(){
        Customer john = new Customer("John");
        assert "John".equals(john.getUuid());
    }
}